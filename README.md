# stamper

A TUI for timestamping song lyrics and creating LRC files using MPD.

## Usage

Run stamper like this:

```python3.10 stamper.py```

Stamper will check if the currently playing MP3 file contains a USLT tag which contains unsynchronized lyrics.

If not, it will look for a TXT file in the directory of the curretly playing MP3 that has the same file name but a .txt file extension.

The unsychnronized lyrics is loaded from one of these sources or another source can be provided through the ``--input-text`` option.

You can add timestamps by hitting SPACE just before each line is sung. The currently sung line is displayed as bold text.

If you made a mistake, you can scroll back up by hitting ``k``. This sets both the playback and the display to the previously timestamped line.

You can also scroll in the other direction of your already stamped lines by hitting ``j``.

When you have timestamped the entire song and hit SPACE a final time an LRC file will be created that looks like what you saw on the screen and a SYLT tag is written to the MP3 file.

You can quit prematurely by hitting q. No LRC or SYLT tag will be created.

## TODO

* open and correct/improve synchronized lyrics from SYLT tags and LRC files
