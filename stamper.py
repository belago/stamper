import os
import argparse
import curses
from mpd import MPDClient
from mutagen.id3 import ID3, SYLT, Encoding

def parse_args():
    parser = argparse.ArgumentParser(
        description="A TUI for timestamping lyrics using MPD."
    )
    parser.add_argument(
        "--input-text", "-i",
        type=str,
        default=None,
        help="path to text file with unsynced lyrics"
    )
    parser.add_argument(
        "--write-sylt", "-s",
        help="write the synchronized lyrics to the MP3's SYLT tag",
        action="store_true"
    )
    parser.add_argument(
        "--resync-sylt", "-S",
        help="open synchronized lyrics from the SYLT tag",
        action="store_true"
    )
    parser.add_argument(
        "--resync-lrc", "-L",
        help="open synchronized lyrics from the LRC file",
        action="store_true"
    )
    args = parser.parse_args()
    return args

def find_music_dir():
    """Read the location of the music directory from mpd.conf"""
    conf_path = os.path.expanduser("~/.config/mpd/mpd.conf")
    with open(conf_path, "r") as file:
        conf = file.read()
        lines = conf.split("\n")
        for line in lines:
            if "music_directory" in line:
                music_dir_line = line
        music_dir = os.path.expanduser(music_dir_line.split("\"")[-2] + "/")

def find_lyrics(client, input_text):
    """Check if currently playing song has a lyrics file."""
    try:
        client.connect("localhost", 6600)
    except:
        pass
    music_dir = find_music_dir()
    music_path = "/home/bela/Music/" + client.currentsong()['file']
    if input_text == None:
        text_path = music_path.split(".")[0] + ".txt" 
        return music_path, text_path
    else:
        return music_path, input_text

def load_lyrics(txt_path, id3_tags):
    """Read unsychronized lyrics file and return a list of lines."""
    if id3_tags.getall("USLT") != []:
        lyrics = id3_tags.getall("USLT")[0].text
    else:
        with open(txt_path, "r") as file:
            lyrics = file.read()
    lines = lyrics.split("\n")
    lines = [line for line in lines if line.strip() != ""]
    return lines

def make_stamp(time):
    """Make a time stamp of format [MM:SS.cc] from seconds."""
    mins = time // 60
    secs = time % 60
    stamp = "[%02d:%05.2f]" % (mins, secs)
    return stamp

def add_stamps(txt_list, stamp_list):
    """Merge corresponding string and text line for list of LRC lines."""
    lrc_list = [f"{stamp_list[i]} {txt_list[i]}" for i in range(len(txt_list))]
    return lrc_list

def write_lrc(txt_path, lrc_list):
    """Write LRC file from list of lines."""
    content = "\n".join(lrc_list)
    lrc_path = txt_path.split(".")[0] + ".lrc"
    with open(lrc_path, "w") as file:
        file.write(content)

def draw_screen(rows, cols, i, lrc_list, stdscr):
    """
    Draw LRC lines on the screen depending on progress and window size.
    """
    stdscr.clear()

    middle = round(rows/2)

    # Determine how many lines are above and below the middle row.
    above = min(i, rows - (rows - middle))
    below = min(len(lrc_list) - i, rows - middle)

    # Collect all lines that fit into fitting_lines.
    left_ind = i - above
    right_ind = i + below
    fitting_lines = lrc_list[left_ind:right_ind]

    # top is the top of the text block.
    # It starts in the middle and goes up until the top row.
    top = max(middle - i, 0)
	
    # Print all lyrics lines that fit in the window.
    for j in range(len(fitting_lines)):
        stdscr.addstr(top + j, 0, fitting_lines[j][:cols-1])

    # Make the current line in the middle bold.
    # Do not add this line to the initial screen.
    if i != -1:
        stdscr.addstr(middle, 0, lrc_list[i][:cols], curses.A_BOLD)
        stdscr.refresh()

def get_time(client):
    """Connect to MPD channel and get elapsed time of current song."""
    try:
        client.connect("localhost", 6600)
    except:
        pass
    time = float(client.status()['elapsed'])
    return time

def jump_to_time(client, time):
    """
    Connect to MPD channel and move playback to last timestamp position.
    """
    try:
        client.connect("localhost", 6600)
    except:
        pass
    client.seekcur(time)

def write_sylt(music_path, sylt_list):
    tags = ID3(music_path)
    tags.setall("SYLT", [SYLT(encoding=Encoding.UTF8, format=2, type=1,
                              text=sylt_list)])
    tags.save(v2_version=3)

def main(stdscr):
    args = parse_args()
    # Set the first timepoint to skip back to at 0 seconds.
    client = MPDClient()
    music_path, txt_path = find_lyrics(client, args.input_text)
    id3_tags = ID3(music_path)
    txt_list = load_lyrics(txt_path, id3_tags)
    stamp_list = ["[00:00.00]" for line in txt_list]
    lrc_list = add_stamps(txt_list, stamp_list)
    sylt_list = [0 for i in range(len(txt_list))]
    curses.use_default_colors()
    rows, cols = stdscr.getmaxyx()
    draw_screen(rows, cols, -1, lrc_list, stdscr)
    i = 0
    save_stops = [0 for i in range(len(txt_list) + 1)]
    # Wait for user input to add timestamp.
    while i <= len(txt_list):
        rows, cols = stdscr.getmaxyx()
        inp = stdscr.getch()
        
        if inp == ord(" "):
            if i < len(txt_list):
                time = get_time(client)
                save_stops[i+1] = time
                stamp = make_stamp(time)
                stamp_list[i] = stamp
                lrc_list = add_stamps(txt_list, stamp_list)
                sylt_list[i] = (txt_list[i], int(time*1000))
                draw_screen(rows, cols, i, lrc_list, stdscr)
            i += 1
        
        elif inp == ord("k"):
            if i > 0:
                previous_time = save_stops[i-1]
                jump_to_time(client, previous_time)
                i += -1
                draw_screen(rows, cols, i-1, lrc_list, stdscr)

        elif inp == ord("j"):
            if i < len(txt_list) and save_stops[i+1] != 0:
                next_time = save_stops[i+1]
                jump_to_time(client, next_time)
                i += 1
                draw_screen(rows, cols, i-1, lrc_list, stdscr)

        elif inp ==  ord("q"):
            quit()
    
    write_sylt(music_path, sylt_list)
    write_lrc(txt_path, lrc_list)    

curses.wrapper(main)
